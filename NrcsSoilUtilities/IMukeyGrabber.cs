﻿
namespace NrcsSoilUtilities
{
    public interface IMukeyGrabber
    {
        int GetMukeyFromLatLon(double latitude, string longitude);
    }
}
