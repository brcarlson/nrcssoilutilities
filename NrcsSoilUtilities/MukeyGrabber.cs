﻿using System;
using System.IO;
using EGIS.ShapeFileLib;

namespace NrcsSoilUtilities
{
    /// <summary>
    /// Gets a NRCS Map Unit Key from a STATSGO database that is saved 
    /// in the shapefile format
    /// </summary>
    public class MukeyGrabber
    {
        const int ARRAY_INDEX_OF_MUKEY = 3;

        private ShapeFile shapeFile;

        // Constructors
        public MukeyGrabber(string pathToShapeFile)
        {
            // Check if the file exists and is a shapefile
            if (!(File.Exists(pathToShapeFile)) ||
                (Path.GetExtension(pathToShapeFile) != ".shp"))
            {
                throw new ArgumentException(
                    "The specified path does not point to a shapefile");
            }

            this.shapeFile = new ShapeFile(pathToShapeFile);
        }

        /// <summary>
        /// Queries a shapefile for the NRCS STATSGO map unit key that occurs
        /// at the location specified by latitude and longitude.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public int GetMukeyFromLatLon(double latitude, double longitude)
        {
            // Ensure lat and lon are inside the bounds of the shapefile
            RectangleD extent = shapeFile.Extent;

            // EasyGIS.NET has lat increase as it reaches the bottom
            if (latitude > extent.Bottom || latitude < extent.Top)
            {
                throw new ArgumentException(
                    "Specified latitude is outside the bounds of the shapefile");
            }

            if (longitude > extent.Right || longitude < extent.Left)
            {
                throw new ArgumentException(
                    "Specified longitude is outside the bounds of the shapefile");
            }

            // Get the index of the feature at specified point
            int featureIndex = shapeFile.GetShapeIndexContainingPoint(
                new PointD(longitude, latitude), 0);

            // Get a list of attributes of the specified feature
            string[] featureAttributes = shapeFile.GetAttributeFieldValues(
                featureIndex);

            string mukey = featureAttributes[ARRAY_INDEX_OF_MUKEY];

            return int.Parse(mukey);
        }
    }
}
