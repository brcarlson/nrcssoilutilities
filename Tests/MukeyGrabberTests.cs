﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NrcsSoilUtilities;

namespace Tests
{
    // MukeyGrabberTestData.shp

    //      Lat: 46.516932
    //      Lon: -119.437030
    //      FID: 30
    //      Shape: Polygon

    //      Attributes:
    //      --------------
    //      AREASYMBOL: US
    //      SPATIALVER: 2
    //      MUSYM s8381
    //      MUKEY: 675761

    [TestClass]
    public class MukeyGrabberTests
    {
        [TestMethod]
        public void ShouldReturnErrorIfPathToShapeFileIsInvalid()
        {
            // Arrange
            MukeyGrabber sut1;
            MukeyGrabber sut2;
            ArgumentException exception1 = new ArgumentException();
            ArgumentException exception2 = new ArgumentException();
            string currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            // Act
            try
            {
                // Test correct extension but file that does not exist
                sut1 = new MukeyGrabber("blah.shp");
            }
            catch (ArgumentException e)
            {
                exception1 = e;
            }

            try
            {
                // Test file that exists but that is not a shapefile
                sut2 = new MukeyGrabber(
                    currentDirectory + @"\MukeyGrabber.dll");
            }
            catch (ArgumentException e)
            {
                exception2 = e;
            }

            // Assert
            Assert.AreEqual(exception1.Message,
                "The specified path does not point to a shapefile");
            Assert.AreEqual(exception2.Message,
                "The specified path does not point to a shapefile");
        }

        [TestMethod]
        public void ShouldThrowErrorIfInvalidLatitude()
        {
            // Arrange
            double tooLargeLatitude = 200;
            double tooSmallLatitude = -180;
            double longitude = -119.437030;

            ArgumentException exception1 = new ArgumentException();
            ArgumentException exception2 = new ArgumentException();

            MukeyGrabber sut = new MukeyGrabber(
                @"..\..\..\Tests\Resources\MukeyGrabberTestData.shp");

            // Act
            try
            {
                // Check if throws error when latitude is too large
                int mukey = sut.GetMukeyFromLatLon(tooLargeLatitude, longitude);
            }
            catch (ArgumentException e)
            {
                exception1 = e;
            }

            try
            {
                // Check if throws error when latitude is too small
                int mukey = sut.GetMukeyFromLatLon(tooSmallLatitude, longitude);
            }
            catch (ArgumentException e)
            {
                exception2 = e;
            }

            // Assert
            Assert.AreEqual(exception1.Message,
                "Specified latitude is outside the bounds of the shapefile");
            Assert.AreEqual(exception2.Message,
                "Specified latitude is outside the bounds of the shapefile");
        }

        [TestMethod]
        public void ShouldThrowErrorIfInvalidLongitude()
        {
            // Arrange
            double tooLargeLongitude = -40;
            double tooSmallLongitude = -190;
            double latitude = 46.516932;

            ArgumentException exception1 = new ArgumentException();
            ArgumentException exception2 = new ArgumentException();

            MukeyGrabber sut = new MukeyGrabber(
                @"..\..\..\Tests\Resources\MukeyGrabberTestData.shp");

            // Act
            try
            {
                // Check if throws error when longitude is too large and
                //is (outside of map bounds)
                int mukey = sut.GetMukeyFromLatLon(latitude, tooLargeLongitude);
            }
            catch (ArgumentException e)
            {
                exception1 = e;
            }

            try
            {
                // Check if throws error when longitude is too small and
                //is (outside of map bounds)
                int mukey = sut.GetMukeyFromLatLon(latitude, tooSmallLongitude);
            }
            catch (ArgumentException e)
            {
                exception2 = e;
            }

            // Assert
            Assert.AreEqual(exception1.Message,
                "Specified longitude is outside the bounds of the shapefile");
            Assert.AreEqual(exception2.Message,
                "Specified longitude is outside the bounds of the shapefile");
        }

        [TestMethod]
        public void ShouldReturnCorrectMukey()
        {
            // Arrange
            int observedMukey;
            int expectedMukey = 675761;
            double lat = 46.516932;
            double lon = -119.437030;

            MukeyGrabber sut = new MukeyGrabber(
                @"..\..\..\Tests\Resources\MukeyGrabberTestData.shp");

            // Act
            observedMukey = sut.GetMukeyFromLatLon(lat, lon);

            // Assert
            Assert.AreEqual(expectedMukey, observedMukey);
        }
    }
}
